(ns clj.keiri.db
  (:require [clojure.spec.alpha :as s]
            [datahike.api :as d]))

(s/def core-types #{:agent :project :offer :customer :task :task-group :invoice})

(s/def :agent/email string?)
(s/def ::agent (s/keys :req [:agent/email]))

(defn validate-input [input type]
  (let [(-> input
            (dissoc :type)
            (assoc :db/id (d/tempid -1)))]))

(defmulti create :type)

(defmutli read :type)

(defmutli update :type)

(defmulti delete :type)
