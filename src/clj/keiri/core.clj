(ns keiri.core
  (:require [datahike.api :as d]
            [clojure.java.io :as io]))

(def data-folder "/tmp/keiri/data")

(def uri (str "datahike:file://" data-folder))


(def schema {:user/email         {:db/cardinality :db.cardinality/one
                                  :db/unique      :db.unique/identity}})

(comment
  (when (not (.exists (io/file data-folder)))
    (io/make-parents data-folder))

  ;; (re)create database
  (d/delete-database uri)
  (d/create-database-with-schema uri schema)
  )


(def conn (d/connect uri))

(d/transact conn [{:db/id (d/tempid -1)
                   :user/name "Konrad"
                   :user/email "konrad@lambdaforge.io"
                   :user/type :user.type/comrade}])

(d/pull @conn '[*] [:user/email "konrad@lambdaforge.io"])
